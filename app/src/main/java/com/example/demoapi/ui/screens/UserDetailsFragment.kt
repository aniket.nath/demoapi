package com.example.demoapi.ui.screens

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.example.demoapi.R
import com.example.demoapi.ui.viewmodels.UserInfoViewModel
import org.koin.android.viewmodel.ext.android.sharedViewModel

class UserDetailsFragment : Fragment() {
    private val viewModel: UserInfoViewModel by sharedViewModel()
    private val args: UserDetailsFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_user_details, container, false)
        val userInfo = viewModel.getUserInfo(args.position)
        return rootView
    }
}