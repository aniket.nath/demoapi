package com.example.demoapi.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoapi.datalayer.entity.UserInfo
import com.example.demoapi.datalayer.repository.UsersInfoRepository
import com.example.demoapi.datalayer.utils.AsyncResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserInfoViewModel(private val usersInfoRepository: UsersInfoRepository): ViewModel() {
    private var userInfo: List<UserInfo> = emptyList()
    private val _userInfoLiveData: MutableLiveData<List<UserInfo>> = MutableLiveData(emptyList())
    val userInfoLiveData: LiveData<List<UserInfo>> = _userInfoLiveData

    private val _loading: MutableLiveData<Boolean> = MutableLiveData(true)
    val loading: LiveData<Boolean> = _loading

    private val _error: MutableLiveData<String> = MutableLiveData("")
    val error: LiveData<String> = _error
    
    fun getUserInfo() {
        CoroutineScope(Dispatchers.IO).launch { 
            usersInfoRepository.getUserDetails().let {response ->
                withContext(Dispatchers.Main) {
                    when(response) {
                        is AsyncResult.Loading -> {
                            _loading.postValue(true)
                        }
                        is AsyncResult.Success -> {
                            _loading.postValue(false)
                            response.data?.let {
                                userInfo = it
                                _userInfoLiveData.postValue(it)
                            }
                        }
                        is AsyncResult.Error -> {
                            _loading.postValue(false)
                            response.message?.let {
                                _error.postValue(it)
                            }?: kotlin.run { _error.postValue("Something went wrong") }
                        }
                    }
                }
            }
        }
    }

    fun getUserInfo(position: Int): UserInfo {
        return userInfo[position]
    }
}