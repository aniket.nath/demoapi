package com.example.demoapi.datalayer.utils

import retrofit2.HttpException
import java.net.SocketTimeoutException

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

class ResponseHandler {
    fun <T : Any> handleSuccess(data: T): AsyncResult<T> {
        return AsyncResult.Success(data)
    }

    fun <T : Any> handleException(e: Exception): AsyncResult<T> {
        return when (e) {
            is HttpException -> AsyncResult.Error(getErrorMessage(e.code()))
            is SocketTimeoutException -> AsyncResult.Error(getErrorMessage(ErrorCodes.SocketTimeOut.code))
            else -> AsyncResult.Error(getErrorMessage(Int.MAX_VALUE))
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Something went wrong"
        }
    }

}