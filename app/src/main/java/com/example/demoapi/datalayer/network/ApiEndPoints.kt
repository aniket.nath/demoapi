package com.example.demoapi.datalayer.network

object ApiEndPoints {
    const val BASE_URL = "https://jsonplaceholder.typicode.com/"
}