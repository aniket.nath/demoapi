package com.example.demoapi.datalayer.utils

sealed class AsyncResult<T>(val data: T? = null, val message: String? = null) {
    class Success<T>(data: T) : AsyncResult<T>(data)
    class Loading<T>() : AsyncResult<T>()
    class Error<T>(message: String) : AsyncResult<T>(message = message)
}
