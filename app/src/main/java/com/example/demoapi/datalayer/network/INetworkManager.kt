package com.example.demoapi.datalayer.network

import com.example.demoapi.datalayer.entity.UserInfo
import retrofit2.http.GET

interface INetworkManager {
    @GET("posts")
    suspend fun getUsers(): List<UserInfo>
}