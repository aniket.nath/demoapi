package com.example.demoapi.ui.screens.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

open class BaseActivity : AppCompatActivity() {
    private var progressBarFragment: ProgressBarFragment?= null
    private var isProgress: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        progressBarFragment = ProgressBarFragment()
    }

    fun showLoading() {
        try {
            progressBarFragment?.let {
                if (!it.isAdded && !it.isVisible && !isProgress) {
                    isProgress = true
                    it.show(supportFragmentManager, "");
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hideLoading() {
        try {
            progressBarFragment?.let {
                it.dismiss()
                isProgress = false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}