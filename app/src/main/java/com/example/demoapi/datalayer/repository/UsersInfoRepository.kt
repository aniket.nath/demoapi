package com.example.demoapi.datalayer.repository

import com.example.demoapi.datalayer.entity.UserInfo
import com.example.demoapi.datalayer.network.INetworkManager
import com.example.demoapi.datalayer.utils.AsyncResult
import com.example.demoapi.datalayer.utils.ResponseHandler

class UsersInfoRepository(
    private val iNetworkManager: INetworkManager,
    private val responseHandler: ResponseHandler
) {
    suspend fun getUserDetails(): AsyncResult<List<UserInfo>> {
        return try {
            val response = iNetworkManager.getUsers()
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}