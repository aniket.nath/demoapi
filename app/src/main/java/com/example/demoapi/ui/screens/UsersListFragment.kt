package com.example.demoapi.ui.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.demoapi.R
import com.example.demoapi.databinding.FragmentUsersListBinding
import com.example.demoapi.ui.extentions.hideLoading
import com.example.demoapi.ui.extentions.showLoading
import com.example.demoapi.ui.viewmodels.UserInfoViewModel
import org.koin.android.viewmodel.ext.android.sharedViewModel

class UsersListFragment : Fragment() {
    private val viewModel: UserInfoViewModel by sharedViewModel()
    private lateinit var binding: FragmentUsersListBinding
    private val navController: NavController by lazy {
        findNavController()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUsersListBinding.inflate(
            inflater,
            container,
            false)
        initApi()
        observeLiveData()
        return binding.root
    }

    private fun observeLiveData() {
        viewModel.loading.observe(requireActivity(), {
            if (it) {
                showLoading()
            } else {
                hideLoading()
            }
        })
        viewModel.userInfoLiveData.observe(requireActivity(), {
            if (it.isNotEmpty()) {
            }
        })
        viewModel.error.observe(requireActivity(), {

        })
    }

    private fun initApi() {
        viewModel.getUserInfo()
    }
}