package com.example.demoapi.ui.extentions

import androidx.fragment.app.Fragment
import com.example.demoapi.ui.screens.common.BaseActivity

fun Fragment.showLoading() {
    activity?.let {
        if (activity is BaseActivity) {
            (activity as BaseActivity).showLoading();
        }
    }
}

fun Fragment.hideLoading() {
    activity?.let {
        if (activity is BaseActivity) {
            (activity as BaseActivity).hideLoading();
        }
    }
}