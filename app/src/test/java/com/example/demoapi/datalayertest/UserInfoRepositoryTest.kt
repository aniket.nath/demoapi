package com.example.demoapi.datalayertest

import com.example.demoapi.datalayer.entity.UserInfo
import com.example.demoapi.datalayer.network.INetworkManager
import com.example.demoapi.datalayer.repository.UsersInfoRepository
import com.example.demoapi.datalayer.utils.AsyncResult
import com.example.demoapi.datalayer.utils.ResponseHandler
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class UserInfoRepositoryTest {
    private val responseHandler = ResponseHandler()
    private lateinit var iNetworkManager: INetworkManager
    private lateinit var repository: UsersInfoRepository
    private val userInfo = UserInfo(1, 1, "Aniket", "Nath")
    private val userInfoList: MutableList<UserInfo> = ArrayList()
    private val userPostResponse = AsyncResult.Success(userInfoList)

    @Before
    fun setup() {
        iNetworkManager = mock()
        userInfoList.add(userInfo)
        runBlocking {
            whenever(iNetworkManager.getUsers()).thenReturn(userInfoList)
        }
        repository = UsersInfoRepository(iNetworkManager, responseHandler)
    }

    @Test
    fun `test get user info from user repository`() = run {
        runBlocking {
            assertEquals(userPostResponse.data, repository.getUserDetails().data)
        }
    }
}