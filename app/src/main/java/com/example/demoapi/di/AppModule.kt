package com.example.demoapi.di

import com.example.demoapi.datalayer.network.AuthInterceptor
import com.example.demoapi.datalayer.network.RetrofitClient.provideLoggingInterceptor
import com.example.demoapi.datalayer.network.RetrofitClient.provideOkHttpClient
import com.example.demoapi.datalayer.network.RetrofitClient.provideRetrofit
import com.example.demoapi.datalayer.network.RetrofitClient.provideUsersApi
import com.example.demoapi.datalayer.repository.UsersInfoRepository
import com.example.demoapi.datalayer.utils.ResponseHandler
import com.example.demoapi.ui.viewmodels.UserInfoViewModel
import org.koin.dsl.module

val networkModule = module {
    factory { AuthInterceptor() }
    factory { provideOkHttpClient(get(), get()) }
    factory { provideLoggingInterceptor() }
    single { provideRetrofit(get()) }
    factory { provideUsersApi(get()) }
    factory { ResponseHandler() }
}

val usersRepositoryModule = module {
    factory { UsersInfoRepository(get(), get()) }
}

val viewModelModule = module {
    factory { UserInfoViewModel(get()) }
}


