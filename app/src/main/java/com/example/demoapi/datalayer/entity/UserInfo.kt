package com.example.demoapi.datalayer.entity

import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("userId") var userId: Int = 0,
    @SerializedName("id") var id: Int = 0,
    @SerializedName("title") var title: String = "",
    @SerializedName("body") var body: String = ""
)